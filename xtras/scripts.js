<!--

//======================================================================
//Toggle menu

// score = 0 div
jQuery(document).ready(function(){
    jQuery('#togglemenu').on('click', function(event) {        
        jQuery('#options').toggle('hide');
        if ($.trim($(this).text()) === 'Hide options') { $(this).text('Show options'); } else { $(this).text('Hide options'); } 
    });
});	

//======================================================================
//Toggle Mathjax


var isTypeset = false;
function ToggleMJ(button) {
  button.disabled = true;
  button.value = (isTypeset ? "Enable Mathjax" : "Disable Mathjax");
  MathJax.Hub.Queue(
    (isTypeset ? RemoveMJ : ["Typeset",MathJax.Hub]),
    function () {
      isTypeset = !isTypeset;
      button.disabled = false;
    }
  );
}

function RemoveMJ() {
  var jax = MathJax.Hub.getAllJax();
  for (var i = 0, m = jax.length; i < m; i++) {
    var tex = jax[i].originalText;
    var isDisplay = (jax[i].root.Get("display") === "block");
    if (isDisplay) tex = "$$"+tex+"$$"; else tex = "$"+tex+"$";
    var script = jax[i].SourceElement();
    jax[i].Remove();
    var preview = script.previousSibling;
    if (preview && preview.className === "MathJax_Preview") 
      preview.parentNode.removeChild(preview);
    script.parentNode.insertBefore(document.createTextNode(tex),script);
    script.parentNode.removeChild(script);
  }
}

//-----------------------------------------


function myFunction_filter() {
    var input, filter, table, tr, td, i;
    input1 = document.getElementById("myInput1");
    filter1 = input1.value.toUpperCase().split(" ");
    input2 = document.getElementById("myInput2");
    filter2 = input2.value.toUpperCase().split(" ");
    input3 = document.getElementById("myInput3");
    filter3 = input3.value.toUpperCase().split(" ");
    input4 = document.getElementById("myInput4");
    filter4 = input4.value.toUpperCase().split(" ");
    input5 = document.getElementById("myInput5");
    filter5 = input5.value.toUpperCase().split(" ");
    input6 = document.getElementById("myInput6");
    filter6 = input6.value.toUpperCase().split(" ");
    input7 = document.getElementById("myInput7");
    filter7 = input7.value.toUpperCase().split(" ");

    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
	td1 = tr[i].getElementsByTagName("td")[0];
	td2 = tr[i].getElementsByTagName("td")[1];
	td3 = tr[i].getElementsByTagName("td")[2];
	td4 = tr[i].getElementsByTagName("td")[3];
	td5 = tr[i].getElementsByTagName("td")[4];
	td6 = tr[i].getElementsByTagName("td")[5];
	td7 = tr[i].getElementsByTagName("td")[6];
	if (td1&&td2&&td3&&td4&&td5&&td6&&td7) {
	    match=true;
	    for (j = 0; j < filter1.length; j++) {
		if (td1.innerHTML.toUpperCase().indexOf(filter1[j]) == -1) {
		    match=false;
		}
	    }
	    for (j = 0; j < filter2.length; j++) {
		if (td2.innerHTML.toUpperCase().indexOf(filter2[j]) == -1) {
		    match=false;
		}
	    }
	    for (j = 0; j < filter3.length; j++) {
		if (td3.innerHTML.toUpperCase().indexOf(filter3[j]) == -1) {
		    match=false;
		}
	    }	   
	    for (j = 0; j < filter4.length; j++) {
		if (td4.innerHTML.toUpperCase().indexOf(filter4[j]) == -1) {
		    match=false;
		}
	    }	 
	    for (j = 0; j < filter5.length; j++) {
		if (td5.innerHTML.toUpperCase().indexOf(filter5[j]) == -1) {
		    match=false;
		}
	    }	 
	    for (j = 0; j < filter6.length; j++) {
		if (td6.innerHTML.toUpperCase().indexOf(filter6[j]) == -1) {
		    match=false;
		}
	    }	 
	    for (j = 0; j < filter7.length; j++) {
		if (td7.innerHTML.toUpperCase().indexOf(filter7[j]) == -1) {
		    match=false;
		}
	    }	 
	    tr[i].style.display = (match ? "" : "none");
	}       
    }
}

function myFunction_filterclean () {
    input1 = document.getElementById("myInput1");
    input1.value = "";
    input2 = document.getElementById("myInput2");
    input2.value = "";
    input3 = document.getElementById("myInput3");
    input3.value = "";
    input4 = document.getElementById("myInput4");
    input4.value = "";
    input5 = document.getElementById("myInput5");
    input5.value = "";
    input6 = document.getElementById("myInput6");
    input6.value = "";
    input7 = document.getElementById("myInput7");
    input7.value = "";
    
    myFunction_filter();
}


-->
    
