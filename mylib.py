
#--------------------------------------------------------------------
#--------------------------------------------------------------------
#--------------------------------------------------------------------

#arrays

#----------------------------------------------------------------- 
def findindices(listinput, value, first=True):
    #returning array even if single element
    if first:
        return [listinput.index(value)]
    else:
        return [[i for i, x in enumerate(listinput) if x == value]]
#----------------------------------------------------------------- 
def GetDuplicates(InputList, getIndices=False):
    duplicate = [x for n, x in enumerate(InputList) if x in InputList[:n]]
    if not getIndices:
        return duplicate
    res = {}
    for d in duplicate:
        res.update({d: [n for n, x in enumerate(InputList) if x==d]})
    return res

#--------------------------------------------------------------------
#--------------------------------------------------------------------
#--------------------------------------------------------------------

#strings
import sys
from difflib import SequenceMatcher
import re

#--------------------------------------------------------------------
def StrBetween(string, delimiters):
    #ex: strextract('asdasd [123] sadasd', '[]') : 123
    return string[string.index(delimiters[0])+1:string.index(delimiters[1])]

#--------------------------------------------------------------------
def StrSimilar(s1, s2, getscore = False, threshold=0.75, test=False, method='SequenceMatcher'):
    if method=='SequenceMatcher':
        t = SequenceMatcher(None, s1, s2).ratio()
        if t>threshold:
            if getscore:
                return True, t
            else:
                return True
        else:
            if getscore:
                return False, t
            else:
                return False
    else:
        if (s1=='' and s2!='') or (s2=='' and s1!='') or (s1 is None) or (s2 is None):
            similar = False
        if getscore:
            return similar, 0
        else:
            return similar  
        pattern = re.compile('([^\s\w]|_)+')
        s1 = set(StrRemoveaccents(pattern.sub('', s1)).lower().split(' '))
        s2 = set(StrRemoveaccents(pattern.sub('', s2)).lower().split(' '))
        a1=set(sorted([ss for ss in ''.join(s1)]))
        a2=set(sorted([ss for ss in ''.join(s2)]))
        if test:
            print(s1, s2, s2.intersection(s1), len(s2.intersection(s1)))
            print(a1, a2, a2.intersection(a1), len(a1.intersection(a2)))
            print(min([len(s1), len(s2)]), max([len(a1), len(a2)]))
        similar = False
        score = 0
        if len(s2.intersection(s1))>=min([len(s1), len(s2)]) or len(a1.intersection(a2))>=max([len(a1), len(a2)]):
            similar = True
            score = len(s2.intersection(s1))/min([len(s1), len(s2)]) + len(a1.intersection(a2))/max([len(a1), len(a2)])
        if getscore:
            return similar, score
        else:
            return similar

#--------------------------------------------------------------------
def dict2string(dictvar):
    return '''
    {}
    '''.format('\n'.join('{}: {}'.format(key, val) for key, val in sorted(dictvar.items())))

#-----------------------------------------------------------------
def bstyles(bstyle): #http://jafrog.com/2013/11/23/colors-in-terminal.html
    res = {
        '': '\033[92m', #green
        ':': '\033[94m', #blue
        '!': '\033[93m', #yellow
        '!!!': '\033[91m', #purple
        '@': '\033[95m', #violet
        '?': '\033[96m', #cyan
        'ENDC': '\033[0m'
        #'BOLD = '\033[1m'
        #'UNDERLINE = '\033[4m'
    }.get(bstyle, '')    # 9 is default if x not found
    return res

#-----------------------------------------------------------------
def StrAnsicol(stringvalue, bstyle):
    #info: y
    #question: c
    #help/hint: b
    #warning: p, v
    if sys.stdout.isatty(): #only if in a terminal
        return bstyles(bstyle) + stringvalue + bstyles('ENDC')
    else:
        return stringvalue
    
#-----------------------------------------------------------------
def printc(stringvalue, bstyle):
    print(StrAnsicol(stringvalue,bstyle))

#--------------------------------------------------------------------
def latex2unicode(stringvalue):
    from bibtexparser.latexenc import unicode_to_crappy_latex1,unicode_to_crappy_latex2, unicode_to_latex
    import itertools
    """
    Convert accent from latex to unicode style.

    :param record: the record.
    :type record: dict
    :returns: dict -- the modified record.
    """
    if '\\' in stringvalue or '{' in stringvalue:
        for k, v in itertools.chain(unicode_to_crappy_latex1, unicode_to_latex):
            if v in stringvalue:
                stringvalue = stringvalue.replace(v, k)

    # If there is still very crappy items
    if '\\' in stringvalue:
        for k, v in unicode_to_crappy_latex2:
            if v in stringvalue:
                parts = stringvalue.split(str(v))
                for key, stringvalue in enumerate(parts):
                    if key+1 < len(parts) and len(parts[key+1]) > 0:
                        # Change order to display accents
                        parts[key] = parts[key] + parts[key+1][0]
                        parts[key+1] = parts[key+1][1:]
                stringvalue = k.join(parts)
    return stringvalue.replace('}','').replace('{','')

#--------------------------------------------------------------------
#--------------------------------------------------------------------
#--------------------------------------------------------------------

#network
import subprocess
import requests
from urllib.parse import urlencode

#--------------------------------------------------------------
def ping(hostname, verbose=False, domain=False):
    if domain:
        hn = hostname[hostname.index('://')+3:] if '://' in hostname else hostname
        try:
            response = subprocess.check_output(['nmap', '-Pn', '-p 22', '-T5', hn])
        except subprocess.CalledProcessError as e:
            print('Error...')
            print(e.output)
            return False
            
        if '1 host up' in str(response):
            if verbose:
                print('{} is up'.format(hn))
            return True
        else:
            if verbose:
                print('{} is down'.format(hn))
            return False
    else: #web page or domain
        if hostname[0:4]!='http':
            hostname = 'http://' + hostname
        # try:
        #     res = urllib.request.urlopen(hostname)
        #     finalurl = res.geturl() #after redirections
        #     request = requests.get(finalurl)
        #     if request.status_code == 200:
        #         return True
        #     else:
        #         return False        
        # except:
        #     return False
        
        # from: https://github.com/pcubillos/bibmanager/blob/master/bibmanager/pdf_manager/pdf_manager.py
        # This fixed MNRAS requests and CAPTCHA issues:
        # (take from https://stackoverflow.com/questions/43165341)
        headers = requests.utils.default_headers()
        headers['User-Agent'] = (
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 '
            '(KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36')
        try:
            request = requests.get(hostname, headers=headers)
            if request.status_code == 200:
                if verbose:
                    print('200')
                return True
            else:
                if verbose:
                    print('Not 200')
                return False  
        except requests.exceptions.ConnectionError:
            if verbose:
                print('Failed to establish a web connection.')
            return False     
            
    
#--------------------------------------------------------------------
#--------------------------------------------------------------------
#--------------------------------------------------------------------

#sys
import readline
from pathlib import Path
import os
import glob

#--------------------------------------------------------------------
#autocompletion from anywhere
#https://gist.github.com/iamatypeofwalrus/5637895
def inputAC(prompt):
    
    class tabCompleter(object):
        """ 
        A tab completer that can either complete from
        the filesystem or from a list.
        
        Partially taken from:
        http://stackoverflow.com/questions/5637124/tab-completion-in-pythons-raw-input
        """
        
        def pathCompleter(self, text, state):
            """  
            This is the tab completer for systems paths.
            Only tested on *nix systems
            """
            
            text = text.replace('~', str(Path.home()))
            line   = readline.get_line_buffer().split()

            res = [x for x in glob.glob(text+'*')][state]
            if os.path.isdir(res):
                res += '/'
            #print(res)
            return res
    
    t = tabCompleter()
    
    readline.set_completer_delims('\t')
    readline.parse_and_bind("tab: complete")
    
    readline.set_completer(t.pathCompleter)
    ans = input(prompt)
    return ans

#--------------------------------------------------------------------
def inputRL(prompt, prefill=''):
    #readline input
    readline.set_startup_hook(lambda: readline.insert_text(prefill))
    try:
        return input(prompt)
    finally:
        readline.set_startup_hook()

#--------------------------------------------------------------------
def FileExists(file):
    testfile = Path(file)
    if testfile.is_file():
        return True
    else:
        return False

#--------------------------------------------------------------------
#--------------------------------------------------------------------
#--------------------------------------------------------------------

#pubs
import subprocess
import ads
import pickle
from urllib.request import urlopen,Request
#from urllib.parse import urlencode
#from urllib.error import HTTPError

#----------------------------------------------------------------- 
def HAL_find(title=None, doi=None, verbose=False):
    baseurl = 'https://hal.archives-ouvertes.fr/search/index/'

    if title is not None:
        p = { 'qa[title_t][]': title }
        
    #replace with doi if available
    if doi is not None:
        if doi!='':
            p = { 'qa[doiId_s][]': doi.replace(':', '\:') }

    p.update({'submit_advanced': 'Rechercher'}) #, 'submitType_s': 'notice'
    
    query = baseurl + "?" + urlencode(p)
    if verbose:
        print(p)
        print(query)
    try:
        q = Request(query)
        #following header parameters to avoid 404 in some cases
        #tests possible with wget
        #q.add_header('Referer', baseurl)
        #q.add_header('user-agent', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1')
        with urlopen(q) as response:
            res = response.read().decode('ISO-8859-1')
    except HTTPError as e:
        print('Problem with query...')
        print(query)
        print(e)
        return None, None

    res = res.lower().replace('\n', '').replace('\r', '').replace('\t', '')
    
    #occ = list(find_all(res, 'href="/hal-'))
    #if occ == []:
    #    print('nope')
    #    return    
    #for o in occ:
    #    print(res[o+7:o+19])

    if 'href="/hal-' in res:
        rescheck = 'href="/hal-'
    elif 'href="/cea-' in res:
        rescheck = 'href="/cea-'
    elif 'href="/tel-' in res:
        rescheck = 'href="/tel-'
    else:
        if verbose:
            print('No result with HAL...')
        return "", ""
        
    o = res.index(rescheck)

    if verbose:
        print('HAL_ID', res[o+7:o+19])
        print('HAL_VERSION', res[o+20:o+21])
    return res[o+7:o+19], res[o+20:o+21]

#-----------------------------------------------------------------
def ADS_getpapers(name, resfile=None):
    #papers = ads.SearchQuery(q="supernova", sort="citation_count")
    #people = list(ads.SearchQuery(first_author="Reiss, A")) #people[0].author
    #papers = list(ads.SearchQuery(author="Reiss, A")) #papers[0].author
    #papers = list(ads.SearchQuery(aff="*stromlo*")) #papers[0].aff
    #bibcodes = ['1994AAS...185.7506Z', '2001A&A...366...62A']
    #articles = [list(ads.SearchQuery(bibcode=bibcode))[0] for bibcode in bibcodes]
    #https://adsabs.github.io/help/search/comprehensive-solr-term-list
    #import ads.sandbox as ads

    with open('/local/home/vleboute/ownCloud/Config/tokens/ads.token') as f:
        ads.config.token = f.readline().replace('\n', '')  # !! ~/.ads/dev_key overrides this
    fl = ['author', 'id', 'bibcode', 'title', 'citation_count', 'aff', 'pub', 'pubdate', 'issue', 'volume', 'page', 'year', 'doctype', 'arxiv_class', 'bibstem', 'doi', 'first_author', 'property', 'read_count']
    
    #doctype: article, eprint, inproceedings, proceedings, proposal
    
    #papers = list(ads.SearchQuery(author="Lebouteiller, V", year='2017', fl=fl)) +\
    #        list(ads.SearchQuery(author="Lebouteiller, V", year='2018', fl=fl))
    papers = list(ads.SearchQuery(author=name, fl=fl, max_pages=100, sort="date"))
        
    print(len(papers))
    if resfile is not None:
        with open(resfile, 'wb') as f:
            pickle.dump(papers, f)
        
    return papers

#-----------------------------------------------------------------
def ADS_checkrefereed(paper):
    if 'NOT REFEREED' in paper.property:
        return False
    return True

#-----------------------------------------------------------------
def ADS_getmypapers(name='lebouteiller, V', update=False, refereed=False, everything=False, firstauthoronly=False, sort='date'):
    publistfile = '/local/home/vleboute/ownCloud/Python/Library/mylib/PubStats/publist.pickle'
    if update:
        papers = ADS_getpapers(name, resfile=publistfile)
    else:
        if FileExists(publistfile):
            with open(publistfile, 'rb') as f:
                # The protocol version used is detected automatically, so we do not
                # have to specify it.
                print('Loading...')
                papers = pickle.load(f)
        else:
            return None
    #for p in papers:
    #    print(p.title)
    if not everything:
        papers = [p for p in papers if (p.doctype!='catalog') and (p.doctype!='proposal') and ('rratum' not in p.title[0])]
    if refereed:
        papers = [p for p in papers if ADS_checkrefereed(p)]
    if firstauthoronly:
        papers = [p for p in papers if (name.split(',')[0] in p.first_author.lower())]
    if sort=='citation_count':
        papers = sorted(papers, key=lambda k: k.citation_count) #[::-1]
    return papers
