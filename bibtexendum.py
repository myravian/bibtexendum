# coding=utf-8

#!/usr/bin/python

#-----------------------------------------------------------------
#-----------------------------------------------------------------
#-----------------------------------------------------------------
#some settings
global adsmirror, adstoken, LibraryName, defaultbrowser, defaulttags

LibraryName= './bibtexendum.bib'

myname='lebouteiller, v.' #format as you would look for it in ADS

#by order of preference
adsmirrors = ['ui.adsabs.harvard.edu', 'esoads.eso.org', 'cdsads.u-strasbg.fr']
#adsmirrors = ['adsabs.harvard.edu', 'esoads.eso.org', 'cdsads.u-strasbg.fr']

with open('/local/home/vleboute/ownCloud/Config/tokens/ads.token') as f:
    adstoken = f.readline().replace('\n', '')
    
defaultbrowser="xdg-open" #default browser from linux
#defaultbrowser="brave-browser"

#defaulttags = None #if tags are not to be used
defaulttags = {'Concepts':
               {'ISM': ['interstellar', '^ISM', '^PDR', 'photodissociation region*', 'HII region*', 'H+ region*'],
                'IGM': ['intergalactic', 'circumgalactic', '^IGM', '^CGM'],
                'Star Formation': ['protost*', 'proto-st*', 'star-form*', 'star form*', '^SFR', '^YSO', 'young stellar object*'],
                'Metallicity': ['metallicit*','metal-poor*', '^O/H', '^Fe/H', 'Z/H'],
                'Chemistry': ['chemical*','elemental', 'abundances', 'ionic abund*', 'chemistry', 'molecul*', 'atom', 'atoms', 'ion', 'ions', 'nucleosynth*'],
                'Physics': ['collision st*', 'atomic data', 'rate coeff*', 'cross section*'],
                'Compact Objects': ['^ULX', 'black hole*', '^AGN', '^HMXB*', 'seyfert']}}

defaulttags.update({'Methods':
                    {'Observations': ['observation*', 'detect*', 'telescope', 'antenna', 'dish', 'instrument'],
                     'Model': ['model*', 'simulation*', 'calculation*', 'numerical', '*hydrodynam*'],
                     'Theory': []}})

defaulttags.update({'Facilities':
                    {'Spitzer': ['spitzer', '^IRS', '^IRAC', '^MIPS'],
                     'Herschel': ['herschel', '^PACS', '^HIFI'],
                     'ALMA': ['^ALMA'],
                     'Hubble': ['^HST', 'Hubble space telescope', 'Hubble telescope'],
                     'VLT': ['^VLT', 'very large telescope'],
                     'CFHT': ['^CFHT'],
                     'ISO': ['isocam', 'isophot', 'infrared space observatory'],
                     'Chandra': [],
                     'XMM-Newton': [],
                     'JWST': ['JWST', 'James Webb'],
                     'IRAS': ['^IRAS'],
                     'IRAM': ['^IRAM', '^NOEMA', 'pdbi', 'plateau de bur*'],
                     'FUSE': ['^FUSE'],
                     'IUE': ['^IUE'],
                     'SPICA': ['^SPICA'],
                     'AKARI': ['^AKARI'],
                     'Fermi': []}})

defaulttags.update({'Wavelengths':
                    {'Radio': ['radio', 'millimeter']+defaulttags['Facilities']['IRAM']+defaulttags['Facilities']['IRAS']+defaulttags['Facilities']['ALMA'],
                     'UV': ['ultraviolet', '^UV', '^STIS', '^IUE']+defaulttags['Facilities']['FUSE']+defaulttags['Facilities']['IUE'],
                     'Optical': [],
                     'IR': ['infrared', '^IR']+defaulttags['Facilities']['Spitzer']+defaulttags['Facilities']['JWST']+defaulttags['Facilities']['AKARI']+defaulttags['Facilities']['ISO']+defaulttags['Facilities']['Herschel']+defaulttags['Facilities']['SPICA'],
                     'Gamma': ['gamma-ray*', 'gamma ray*']+defaulttags['Facilities']['Fermi'],
                     'X-rays': ['x-ray', '^ULX', '^HMXB']+defaulttags['Facilities']['Chandra']+defaulttags['Facilities']['XMM-Newton']}})

defaulttags.update({'Tracers':
                    {'Spectroscopy': ['spectr*', '^IRS', '^PACS', '^HIFI', '^FTS']+defaulttags['Facilities']['FUSE'],
                     'Photometry': ['photom*'],
                     'CO': ['^CO'],
                     'H2': ['H$_2$', 'H2'],
                     '[CII]': ['^CII', '^C II', '^C ii', '^Cii'],
                     'Cosmic rays': ['cosmic ray*', 'cosmic-ray*'],
                     'Dust': ['dust', 'grain*'],
                     'PAH': ['^PAH*', 'polycyclic aro*']}})

defaulttags.update({'Objects':
                    {'Local': ['milky', '^Galactic'],
                     'Extragalactic': ['galaxies', 'extragal*', '^ULIRG*', '^AGN', 'dwarf gal*', 'quasar', 'spirals', 'seyfert'],
                     'Dwarf galaxies': ['compact dwarf*', 'dwarf gal*'],
                     'Magellanic Clouds': ['^LMC', '^SMC', 'large magell*', 'small magell*', 'magellanic system'],
                     'High-z': ['redshift', 'z']}})



#-----------------------------------------------------------------
#-----------------------------------------------------------------
#-----------------------------------------------------------------
import sys
import argparse
from pathlib import Path
import subprocess
import feedparser
from subprocess import call
import pprint
import re
from urllib.request import urlopen,Request
from urllib.parse import urlencode, quote_plus
from urllib.error import HTTPError
import requests, json
import datetime
import os, shutil
import unicodedata
from operator import itemgetter
from astropy.utils.data import download_file
from copy import deepcopy
import bibtexparser
from bibtexparser.bparser import BibTexParser
#from bibtexparser.bwriter import BibTexWriter
#from bibtexparser.customization import convert_to_unicode
import signal
import logging
from mylib import GetDuplicates, findindices
from mylib import dict2string, StrAnsicol,printc, StrSimilar,\
    latex2unicode, StrBetween
from mylib import ping
from mylib import inputAC, inputRL, FileExists
from astroquery.ned import Ned
from filecmp import cmp
from mylib import ADS_getmypapers, HAL_find

#-----------------------------------------------------------------
# choose ADS mirror

#-----------------------------------------------------------------
def chooseads():
    adsmirror = ''
    for m in adsmirrors:
        #print(m)
        if ping(m, verbose=False, domain=True):
            adsmirror = m
            print('ADS mirror: {}'.format(adsmirror))
            return adsmirror
    if adsmirror=='':
        print('Offline?')
        return ''
#-----------------------------------------------------------------
adsmirror = chooseads()
#if adsmirror=='': # do we continue offline?
#    exit()
#-----------------------------------------------------------------
class DelayedKeyboardInterrupt(object):
    def __enter__(self):
        self.signal_received = False
        self.old_handler = signal.getsignal(signal.SIGINT)
        signal.signal(signal.SIGINT, self.handler)

    def handler(self, sig, frame):
        self.signal_received = (sig, frame)
        logging.debug('SIGINT received. Delaying KeyboardInterrupt.')

    def __exit__(self, type, value, traceback):
        signal.signal(signal.SIGINT, self.old_handler)
        if self.signal_received:
            self.old_handler(*self.signal_received)
#-----------------------------------------------------------------
def search():
    cmd = '\pgrep -fc recoll-webui'
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    n = int(proc.stdout.read1(1).decode('ascii'))
    running = False
    #for line in proc.stdout:
    #    if 'python2.7' in str(line):
    #        print('Listening process already running...')
    #        running = True
    #if not running:
    if n>=2: #somehow in python there are 2 procs
        print('Listening process already running...')
        return
    else:
        print('Running listening process')
        cmd = 'python2.7 recoll-webui/webui-standalone.py >/dev/null 2>&1'
        proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)

    call([defaultbrowser, 'http://localhost:8080/'])
#-----------------------------------------------------------------
def remove_accents(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    only_ascii = nfkd_form.encode('ASCII', 'ignore')
    res = only_ascii.decode("utf-8") #.replace('{','').replace('}','')
    #res = only_ascii.decode("ISO-8859-1")
    return res
#-----------------------------------------------------------------
def nameclean(name):
    #first replace the latex \-character codes
    specialchars = ('oe', 'OE', 'o', 'O', 'i', 'j', 'l', 'L', 'ae', 'AE')
    for s in specialchars:
        if '\\'+s in name:
            name = name.replace('\\'+s, s)
    name = latex2unicode(name).replace('textquoteright' , '') # remove quotes i names
    name = remove_accents(''.join(e for e in name if e.isalnum()))
    return name
#-----------------------------------------------------------------
#def namecleanwithspace(name):
#    return remove_accents(''.join(e for e in name if e.isalnum() or e==' '))
#-----------------------------------------------------------------
def checkbrackets(s):
    return s.count('{')==s.count('}')
#-----------------------------------------------------------------
def fixfields(s):
    p = re.compile(r'month = (\w*),') #fixes things like month = jun, 
    s = p.sub('month = "\g<1>",', s)
    match = re.search(r'(?s)abstract = "{(.*)}",', s)
    try:
        if not checkbrackets(match.group(1)): #replace with empty abstract
            printc('Problem with curly brackets in abstract, removing all curly brackets...', '!!!')
            p = re.compile(r'(?s)abstract = "{(.*)}",') #(?s) is to match even \n
            try:
                s = p.sub('abstract = "{{{}}}",'.format(match.group(1).replace('{','').replace('}','')), s)
            except:
                printc('Problem with regex replacement, now removing all special characters in abstract...', '!!!')
                s = s.replace(match.group(1), re.sub(r'\W+', ' ', match.group(1)))
                #s = p.sub('abstract = "{{{}}}",'.format(match.group(1).replace('{','(').replace('}',')')), s)

            s = s.replace('\mu', 'um')
            #print(s)
            #exit()
        #print(match.group(1))
        #print(s)
    except:
        print('Not fixing anything... no abstract?')        
    return s
#-----------------------------------------------------------------
def getfirstauthor(entry):
    name = entry['author'].split(" and ")[0].replace('~',' ') #.replace('}','').replace('{','').replace('~',' ')
    #if lastname: #return name and initials
    if name.find(',')>0: #consortia?
        last = name[0:name.find(',')]
        initials = name[name.find(','):]
    else:
        #print('Consortium name: {}'.format(name))
        last = name
        initials = ''
        
    return {'full': name, 'last': last, 'initials': initials}
    #else:
    #    return name
#-----------------------------------------------------------------
def createcitekey(entry, entries=None, year=False):
    if 'firstauthor' not in entry.keys(): #happens when importing new entry
        entry['firstauthor'] = getfirstauthor(entry)['full']
    res = nameclean(getfirstauthor(entry)['last'])
    #print('res: {}'.format(getfirstauthor(entry)['last']))
    #print('res: {}'.format(res))
    if year:
        allids = [entries[x]['ID'] for x,b in enumerate(entries)]
        res += str(entry['year'])
        sequence = 'abcdefghijklmnopqrstuvwxyz'
        for s in sequence:
            if (res+s in allids) and (res+s != entry['ID']):
                print(StrAnsicol('ID exists: ','!') + StrAnsicol(res+s,'@'))
                print([x['title'] for x in entries if x['ID']==res+s])
            else:
                return res+s
    return res
#-----------------------------------------------------------------
def getlocalpdfname(entry, get=''):
    #returns local pdf name and corresponding field
    #print(entry['author'])
    directory = PathBiblio + createcitekey(entry) + '/'
    filename = entry['ID'] + '.pdf' 
    libfilename = createcitekey(entry) + '/'+entry['ID'] + '.pdf' #path independent
    filelocation = directory + filename
    fieldvalue = ':'+TildePathBiblio + createcitekey(entry) + '/'+filename+':PDF'
    return {'directory': directory, 'filename': filename, 'libfilename': libfilename, 'filelocation': filelocation, 'fieldvalue': fieldvalue}
#-----------------------------------------------------------------
def getjournalshort(journal, prefix=''):
    res = {
        '\\apjl': 'ApJL',
        '\\apjs': 'ApJS', 
        '\\aap': 'A&A', 
        '\\aaps': 'A&AS', 
        '\\apss': 'Ap&SS', 
        '\\aj': 'AJ', 
        '\\apj': 'ApJ', 
        '\\araa': 'ARA&A', 
        '\\mnras': 'MNRAS', 
        '\\nat': 'Nat.', 
        '\\pasj': 'PASJ',
        '\\pasp': 'PASP',
        '\\pasa': 'PASA',
        '\\rmxaa': 'RMxAA', 
        'Astronomy {&} Astrophysics': 'A&A',
        '\\physrep': 'Phys. Rep.',
        'The Astrophysical Journal': 'ApJ', 
        'Monthly Notices of the Royal Astronomical Society': 'MNRAS',
        'Royal Astronomical Society': 'RAS',
        'Physical Review Letters': 'Phys. Rev. Lett.',
        'ASP Conference Series': 'ASP Conf. Series',
        'Reviews of Modern Physics': 'Rev. Mod. Phys.'
    }.get(journal, journal)    # 9 is default if x not found
    if 'ArXiv' in res: #shorten ArXiv preprint
        res = 'ArXiv'
    if len(res)>15:
        res = res[0:15]
    return res
#-----------------------------------------------------------------
def gethallink(entry):
    url = 'https://hal.archives-ouvertes.fr/'
    if 'hal_id' in entry.keys():
        if entry['hal_id']=='':
            return ''
        res = url + entry['hal_id']
        if 'hal_version' in entry.keys():
            if entry['hal_version']!='':
                res += 'v{}'.format(entry['hal_version'])
        return res
    else:
        return ''
#-----------------------------------------------------------------
def getarxivlink(entry):
    url = 'https://arxiv.org/abs/'
    if 'arxivid' in entry.keys():
        return url+formatarxivid(entry['arxivid'])
    elif 'eprint' in entry.keys():
        return url+formatarxivid(entry['eprint'])
    else:
        return ''
#-----------------------------------------------------------------
def getbibtex(source):
    #print(source)   
    ##debug:
    #response = urlopen(source)
    #res = response.read().decode("ISO-8859-1")
    #r = res[res.find('@'):]
    #print(r)
    i = 0
    tmp = source.replace('adsmirror', adsmirror)
    print('Trying following query: '+tmp)
    try:
        q = Request(tmp)
        #following header parameters to avoid 404 in some cases with ADS (seems pretty random...)
        #tests possible with wget
        q.add_header('Referer', adsmirror)
        q.add_header('user-agent', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1')
        with urlopen(q) as response:
            res = response.read().decode("ISO-8859-1")
            return res[res.find('@'):]
    except HTTPError as e:
        printc('Problem with query...','!!!')
        print(tmp)
        print(e)
        return None
#-----------------------------------------------------------------
#def getparams(entry):
#    if 'bibcode' in entry.keys():
#        if 'arxiv' not in entry['bibcode'].lower():
#            return { 'bibcode': str(entry['bibcode']) }
#    if 'doi' in entry.keys():
#        return { 'doi': str(entry['doi'].split(' ')[0]) }
#    elif 'arxivid' in entry.keys():
#        return { 'arXiv': formatarxivid(entry['arxivid']) }
#    elif 'eprint' in entry.keys():
#        return { 'arXiv': formatarxivid(entry['eprint']) }
#    else:
#        search = '"'+entry['title']+'" '+getfirstauthor(entry)['last']
#        return { 'q': search, 'oq': search}
#-----------------------------------------------------------------
def parsepubcode(inp):
    if type(inp) is dict: #and entry was given
        if 'bibcode' in inp.keys():
            if 'arxiv' not in inp['bibcode'].lower():
                return 'bibcode', inp['bibcode']
        if 'doi' in inp.keys():
            return 'doi', str(inp['doi'].split(' ')[0])
        elif 'arxivid' in inp.keys():
            return 'arxiv', formatarxivid(inp['arxivid'])
        elif 'eprint' in inp.keys():
            return 'arxiv', formatarxivid(inp['eprint'])
    else: # string input
        pubcode = inp.strip()
        if re.search('[a-zA-Z]', pubcode) is not None and len(pubcode)==19: #contains letters and is 19 character long
            printc('ADS bibliographic code','!')
            return 'bibcode', pubcode
        elif '://' in pubcode:
            printc('Bibtex online file','!')
            return 'bibtex', '?'
        elif '/' in pubcode:
            printc('DOI code','!')
            return 'doi', pubcode
        else:
            printc('ArXiv code','!')
            return 'arxiv', formatarxivid(pubcode)
#-----------------------------------------------------------------
def adsquery(inp, get = [], forceold=False):
    if forceold:
        print('Forcing old ADS...')
    
    pubcodetype, pubcode = parsepubcode(inp)
    #print('pubcodetype: {}, pubcode: {}'.format(pubcodetype, pubcode))

    if 'bibcode' in get: #for getting bibtex from ui.adsabs
        if pubcodetype=='bibcode':
            return pubcode
        if 'ui.adsabs' in adsmirror:
            headers = {'Authorization': 'Bearer:{}'.format(adstoken)} 
            query = {pubcodetype: pubcode}
            encoded_query = urlencode(query, quote_via=quote_plus).replace('=', ':')
            #print(encoded_query)
            r = requests.get("https://api.adsabs.harvard.edu/v1/search/query?q={}&fl=bibcode&rows=1".format(encoded_query), headers=headers)
            #print(json.loads(r.content)['response']['docs'])
            try:
                bibcode = json.loads(r.content)['response']['docs'][0]['bibcode']
                print('bibcode: {}'.format(bibcode))
            except:
                bibcode = None
            return bibcode
        else:
            print('Problem getting bibcode...')
            exit()        
    
    if 'abstractlink' in get:
        if 'ui.adsabs' in adsmirror:
            if pubcodetype=='bibcode':
                query = 'https://ui.adsabs.harvard.edu/abs/{}/abstract'.format(pubcode)
            else:
                query = 'https://ui.adsabs.harvard.edu/abs/{}:{}/abstract'.format(pubcodetype, pubcode)
        else:        
            p = { pubcodetype: pubcode }
            query = baseurl + "?" + urlencode(p)
            baseurl = 'http://{}/cgi-bin/bib_query'.format(adsmirror)
            query = query.replace('arxiv=', 'arxiv:').replace('doi=', 'doi:')
        return query
        
    if 'pdflink' in get:
        if 'ui.adsabs' in adsmirror:
            bibcode = adsquery(pubcode, get=['bibcode'])
            if (bibcode is None) or ('arxiv' in bibcode.lower()):
                if pubcodetype=='arxiv':
                    return 'https://arxiv.org/pdf/{}.pdf'.format(formatarxivid(pubcode))
                else:        
                    call([defaultbrowser, "http://{}/cgi-bin/nph-data_query?bibcode={}&link_type=ARTICLE&db_key=AST&high=".format(adsmirror, pubcode)])
                    return ''
            #pdf link not the same for old papers (pre 2000?)
            if ping('https://ui.adsabs.harvard.edu/link_gateway/{}/PUB_PDF'.format(bibcode)):
                query = 'https://ui.adsabs.harvard.edu/link_gateway/{}/PUB_PDF'.format(bibcode)
            else:
                query = 'http://articles.adsabs.harvard.edu/pdf/{}'.format(bibcode)
            return query
        else:                    
            call([defaultbrowser, "http://{}/cgi-bin/nph-data_query?bibcode={}&link_type=ARTICLE&db_key=AST&high=".format(adsmirror, pubcode)])
            return ''

    if 'bibtexlink' in get:
        p = { pubcodetype: pubcode }
        baseurl = 'http://{}/cgi-bin/nph-bib_query'.format(adsmirror)
        p['data_type'] = 'BIBTEXPLUS' #plus for abstracts
        p['db_key'] = 'AST'
        p['nocookieset'] = 1
        query = baseurl + "?" + urlencode(p)
        query = query.replace('arXiv=', 'arXiv:').replace('doi=', 'doi:')
        return query

    if 'bibtex' in get:
        if 'ui.adsabs' in adsmirror and forceold==False:
            bibcode = adsquery(pubcode, get=['bibcode'])
            if bibcode is None:
                return adsquery(pubcode, get=['bibtex'], forceold=True)
            headers = {'Authorization': 'Bearer:{}'.format(adstoken)}
            payload = {'bibcode': bibcode}
            r = requests.post("https://api.adsabs.harvard.edu/v1/export/bibtexabs", data=payload, headers=headers)
            print(bibcode)
            content = r.content
            print(content)
            try:
                content_dict = json.loads(content)
            except:
                print(content)
                exit()
            if 'export' not in content_dict.keys():
                print(content)
                exit()            
            return content_dict['export']
        else:
            adsmirror_tmp = adsmirror
            if forceold and "ui." in adsmirror:
                adsmirror_tmp = "adsabs.harvard.edu"
            p = { pubcodetype: pubcode }
            baseurl = 'http://{}/cgi-bin/nph-bib_query'.format(adsmirror_tmp)
            p['data_type'] = 'BIBTEXPLUS' #plus for abstracts
            p['db_key'] = 'AST'
            p['nocookieset'] = 1
            query = baseurl + "?" + urlencode(p)
            query = query.replace('arxiv=', 'arxiv:').replace('doi=', 'doi:')

            return getbibtex(query)
            
    return   
#-----------------------------------------------------------------
# def getadslink(entry, bibtexentry=False, getcontent=False):
    
#     if type(entry) is dict:
#         #if 'adsurl' in entry.keys() and bibtexentry==False and getcontent==False:
#         #    res = entry['adsurl'] #default
#         #    if 'http' not in entry['adsurl']:
#         #        search = '"'+entry['title']+'" '+getfirstauthor(entry)['last']
#         #        p = { 'q': search, 'oq': search}
#         #        res = 'https://www.google.com/search?' + urlencode(p)
#         #    elif 'journal' in entry.keys():
#         #        if entry['journal'] == 'ignore':
#         #            search = '"'+entry['title']+'" '+getfirstauthor(entry)['last']
#         #            p = { 'q': search, 'oq': search}
#         #            res = 'https://www.google.com/search?' + urlencode(p)
#         #    return {'res': res, 'pdflink': ''}

#         #print(entry['bibcode'].lower(), 'arxiv' not in entry['bibcode'].lower())

#         p = getparams(entry)
#         if 'oq' in p.keys():
#             res = 'https://www.google.com/search?' + urlencode(p)
#             return {'res': res, 'pdflink': ''}

#     else: #string input
#         pubcodetype = parsepubcode(entry)
#         if pubcodetype=='bibcode':
#             p = { 'bibcode': entry }
#         elif pubcodetype=='doi':
#             p = { 'doi': entry }
#         elif pubcodetype=='arxiv': 
#             p = { 'arXiv': formatarxivid(entry) }
#         elif pubcodetype=='bibtex':
#             printc('Bibtex online file','!')
#             pdflink = ''
#             return {'res': getbibtex(entry), 'pdflink': pdflink}

#     #pdflink
#     if ('arXiv' in p.keys()): # or ('arxiv' in p['journal'].lower()):
#         pdflink = 'https://arxiv.org/pdf/'+formatarxivid(p['arXiv'])+'.pdf'
#     elif 'bibcode' in p.keys():        
#         if getcontent:
#             #print("http://{}/cgi-bin/nph-data_query?bibcode={}&link_type=ARTICLE&db_key=AST&high=".format(adsmirror, p['bibcode']))
#             call([defaultbrowser, "http://{}/cgi-bin/nph-data_query?bibcode={}&link_type=ARTICLE&db_key=AST&high=".format(adsmirror, p['bibcode'])])
#         pdflink = ''
#     else:
#         pdflink = ''
#     #printc(pdflink, '!')
    
#     if bibtexentry:
#         baseurl = 'http://adsmirror/cgi-bin/nph-bib_query'
#         p['data_type'] = 'BIBTEXPLUS' #plus for abstracts
#         p['db_key'] = 'AST'
#         p['nocookieset'] = 1
#     else:
#         baseurl = 'http://adsmirror/cgi-bin/bib_query'
        
#     query = baseurl + "?" + urlencode(p)
#     query = query.replace('arXiv=', 'arXiv:').replace('doi=', 'doi:')

#     if getcontent:
#         return {'res': getbibtex(query), 'pdflink': pdflink}
#     else:
#         query = query.replace('adsmirror', adsmirror)
#         return {'res': query, 'pdflink': pdflink}
#-----------------------------------------------------------------
def formatarxivid(inputstr):
    #remove v? from string
    return str( re.sub('v\S', '', inputstr.lower().replace('arxivid=', '')) )
#-----------------------------------------------------------------
def getsource(entry):
    if 'journal' in entry.keys():
        return getjournalshort(entry['journal'])
    if 'booktitle' in entry.keys():
        return getjournalshort(entry['booktitle'])
    return ''
#-----------------------------------------------------------------
def parsefilefield(entry):
    if entry['file'].strip()!='':
        for i,file in enumerate(entry['file'].split(';')):
            if 'online' in file or 'http' in file:
                continue
            #should be a local pdf file
            file = file.replace(':pdf', '').replace(':PDF', '')
            if file[0]==':':
                file=file[1:]
            file = file.replace('{é}', 'é')
            return os.path.expanduser(file)
    return ''
#-----------------------------------------------------------------
def findword(word, text):
    post = '' if word[-1]=='*' else '\\b'
    pre = '' if word[0]=='*' else '\\b'
    ignorecase = False if '^' in word else True
    word = pre+word.replace('*', '').replace('^', '')+post
    if ignorecase:
        regex = re.compile('{}'.format(word), re.IGNORECASE)
    else:
        regex = re.compile('{}'.format(word))
    #print(word, ignorecase, regex.findall(text)) #re.sub(r'\{.*\}', '', text)))
    return regex.findall(text) #re.sub to remove the latex code
#-----------------------------------------------------------------
def tagentry(entry):
    if defaulttags is None:
        printc('defaulttags parameter set to None!','!!!')
        return
    #print(defaulttags.keys())
    tags = ''
    text = entry['title']
    if 'abstract' in entry.keys():
        text += ' ' + entry['abstract']
    for t in defaulttags.keys():
        sugs = ''
        for tt in defaulttags[t].keys():
            sug = False
            if defaulttags[t][tt]==[]:
                tmp = [tt]
            else:
                tmp = defaulttags[t][tt]
            for dd in tmp:
                res = findword(dd, text)
                #print(res)
                if res!=[]:
                    sug = True
            if sug:
                sugs += tt+','
        #if sugs[-1]==',':
        sugs = sugs[:-1] #remove last comma
        #tmp = inputRL(StrAnsicol('Tag/{} [stop:save+exit]: '.format(t), '?'), sugs)
        tags += '{}={{{}}}:'.format(t, sugs)
        #if tmp=='stop':
        #    return 'stop'
    if isreview(entry):
        tags += 'review:'
    entry['tags'] = tags
    print(StrAnsicol(entry['ID'],'@') + ': ' + entry['tags'])
    return ''
#-----------------------------------------------------------------
def tagentries(bib_database):
    if defaulttags is None:
        printc('defaulttags parameter set to None!','!!!')
        return
    empty = ':'.join(['{}=[]'.format(x) for x in defaulttags.keys()])
    print(empty)
    for i,entry in enumerate(bib_database.entries):
        #if empty in entry['tags']:
        #pp = pprint.PrettyPrinter(indent=4)
        #print('\nID: ' + StrAnsicol(entry['ID'], '@'))
        #print('\nTitle: ' + StrAnsicol(entry['title'], '!'))
        #if 'abstract' in entry.keys():
        #    print('\nAbstract: ' + StrAnsicol(entry['abstract'], '!'))
        status = tagentry(entry)
        #if i==0:
        #    return
        #if status=='stop':
        #    return    
#-----------------------------------------------------------------
def makeorgtable(entries, sortkeys, reverse=False, name='', where=None, iwhere=None):
    
    if where is not None:
        print(where)
        
    tmp = sorted(entries, key=sortkeys, reverse=reverse)  #, reverse=True
    
    orgtable = "#+COLUMNS: %15ID %50TITLE %20FIRSTAUTHOR %5YEAR %15SOURCEBIS \n" # #+STARTUP: latexpreview\n
        
    #populate rows
    nresults = 0
    for ipaper,a in enumerate(tmp):
        
        if where is not None:
            if where[1] not in a[where[0]]:
                continue
        
        filelink = ''
        if a['file']!='':
            filelink = "[[{}][{}]]".format(getlocalpdfname(a)['filelocation'],'F')
        
        paperlinks=''
        if a['link']!='':
            for i,link in enumerate(a['link'].split(' ')):
                if 'papers2' not in link:
                    paperlinks += "[[{}][{}]]".format(link,"L"+str(i+1))
        
        warning = ''
        if 'volume' not in a.keys() and 'doi' not in a.keys():
            warning = '!!!' 
            
        arxivlink = getarxivlink(a)
        if arxivlink!='':
            arxivlink = "[[{}][{}]]".format(arxivlink,"X")
            
        #adslink = getadslink(a)['res']
        adslink = adsquery(a, get=['abstractlink'])
        if adslink!='':
            adslink = "[[{}][{}]]".format(adslink,"A")
        
        #adsbibtexlink = getadslink(a, bibtexentry=True)['res']
        adsbibtexlink = adsquery(a, get=['bibtexlink'])
        if adsbibtexlink!='':
            adsbibtexlink = "[[{}][{}]]".format(adsbibtexlink,"B")

        abstract = ''        
        if 'abstract' in a.keys():
            abstract = str(a['abstract'])#.replace('(','\(').replace(')','\)')
            
        showbibtex = dict2string(a)
        showbibtex = showbibtex.replace('\n', '<br>')
        
        sourceinfo = ''
        if 'volume' in a.keys():
            sourceinfo += ' Volume ' + str(a['volume'])
        if 'number' in a.keys():
            sourceinfo += ' Number ' + str(a['number'])
        if 'pages' in a.keys():
            sourceinfo += ' Pages ' + str(a['pages'])
        if 'eprint' in a.keys():
            sourceinfo += ' Eprint ' + str(a['eprint'])
            
        formatted_author = nameclean(getfirstauthor(a)['last']) + getfirstauthor(a)['initials']
                                     
        orgtable += '''* {}
 :PROPERTIES:
 :ID: {}
 :ENTRYTYPE: {}
 :FIRSTAUTHOR: {}
 :AUTHOR: {}
 :YEAR: {}
 :TITLE: {}
 :ABSTRACT: {}
 :SOURCE: {}
 :SOURCEBIS: {}
 :TIMESTAMP: {}
 :FILELINK: {}
 :ADSLINK: {}
 :ARXIVLINK: {}
 :END:\n'''.format(a['ID'], a['ID'], a['ENTRYTYPE'], formatted_author, a['author'].replace('\n', ' '), a['year'], a['title'].replace('\n', ' '), abstract.replace('\n', ' '), sourceinfo, getsource(a), a['timestamp'], filelink, adslink, arxivlink)

        nresults += 1
        
    return orgtable, nresults
#-----------------------------------------------------------------
def maketable(entries, sortkeys, reverse=False, name='', where=None, iwhere=False):
    
    if where is not None:
        print(where)
            
    tmp = sorted(entries, key=sortkeys, reverse=reverse)  #, reverse=True
    
    entrytable = "\n<br><div id='table_"+name+"'><center><table id='myTable' class='sortable'>\
        <thead><tr>\
        <th width=3% class=sortth>Type</th>\
        <th width=0% class=sortth style='display:none;'>First author</th>\
        <th width=10% class=sortth>Author(s)</th>\
        <th width=5% class=sortth>Year</th>\
        <th width=0% class=sortth style='display:none;'>Title</th>\
        <th width=42% class=sortth>Title</th>\
        <th width=0% class=sortth style='display:none;'>Tags</th>\
        <th width=10% class=sortth>Journal</th>\
        <th width=9% class=sortth>Cite</th>\
        <th width=8% class=sortth>Added</th>\
        <th width=2% class=sortth>F</th>\
        <th width=2% class=sortth>A</th>\
        <th width=2% class=sortth>X</th>\
        <th width=2% class=sortth>B</th>\
        <th width=2% class=sortth>H</th>\
        <th width=2% class=sortth>...</th>\
        </tr></thead><tbody>"
        
    #<th width=2% class=sortth>L</th>\
                            
    #populate rows
    nresults = 0
    for ipaper,a in enumerate(tmp):
        
        if where is not None:
            if iwhere is False:
                if where[1] not in a[where[0]]:
                    continue
            else: #inverse
                if a[where[0]] not in where[1]:
                    continue
                
        filelink = ''
        if a['file']!='':
            filelink = "<a href='"+ getlocalpdfname(a)['filelocation'] + "' target=_blank>F</a>"
        
        paperlinks=''
        if a['link']!='':
            for i,link in enumerate(a['link'].split(' ')):
                if 'papers2' not in link:
                    paperlinks += "<a href='"+link+"' target=_blank>L"+str(i+1)+"</a> "
        
        warning = ''
        if 'volume' not in a.keys() and 'doi' not in a.keys():
            warning = '!!!' 
            
        arxivlink = getarxivlink(a)
        if arxivlink!='':
            arxivlink = "<a href='"+arxivlink+"' target=_blank>X</a>"
            
        #adslink = getadslink(a)['res']
        adslink = adsquery(a, get=['abstractlink'])
        if adslink!='':
            adslink = "<a href='"+adslink+"' target=_blank>A</a>"
        
        #adsbibtexlink = getadslink(a, bibtexentry=True)['res']
        adsbibtexlink = adsquery(a, get=['bibtexlink'])
        if adsbibtexlink!='':
            adsbibtexlink = "<a href='"+adsbibtexlink+"' target=_blank>B</a>"

        hallink = gethallink(a)
        if hallink!='':
            hallink = "<a href='"+hallink+"' target=_blank>H</a>"
            
        showbibtex = dict2string(a)
        showbibtex = showbibtex.replace('\n', '<br>')
            
        sourceinfo = ''
        sourceinfo2 = ''
        if 'volume' in a.keys():
            sourceinfo += '<br>Volume ' + str(a['volume'])
            sourceinfo2 += str(a['volume'])
        if 'number' in a.keys():
            sourceinfo += '<br>Number ' + str(a['number'])
        if 'pages' in a.keys():
            sourceinfo += '<br>Pages ' + str(a['pages'])
            sourceinfo2 += ', ' + str(a['pages'])
        if 'eprint' in a.keys():
            sourceinfo += '<br>Eprint ' + str(a['eprint'])
            
        formatted_author = nameclean(getfirstauthor(a)['last']) + getfirstauthor(a)['initials']

        abstract = ''        
        if 'abstract' in a.keys():
            abstract = str(a['abstract']).replace('}','').replace('{','') #.replace('(','\(').replace(')','\)')
            etal = ' et al. ' if a['author'].count(' and ') else ''
            abstract += '<br><br><font color=green>"{}": {} {} {}, {}, {}</font>'.format(a['title'].replace('}','').replace('{',''), formatted_author, etal, a['year'], getsource(a), sourceinfo2)

        if myname.split(',')[0]  in a['author'].lower() :
            trtableclass = 'trtablehighlight'
            if hallink=='':
                hallink='<!>'
                print('https://hal.archives-ouvertes.fr/submit/index')
                if 'doi' in a.keys():
                    print(a['doi'])
                else:
                    print(a['bibcode'])
        else:
            trtableclass = 'trtable'
            
        nresults +=1
        entrytable += "\n<tr class="+trtableclass+">\
            \n<td class=type>" + a['ENTRYTYPE'] + "</td>\
            \n<td style='display:none;'><a class='more author'>" + formatted_author + "</a></td>\
            \n<td><a class='more author'>" + formatted_author + "</a> <div class=extrainfos><br>"+a['author']+"</div></td>\
            \n<td class=year>"+ a['year'] + "</td>\
            \n<td style='display:none;' class=title>"+ a['title'].replace('}','').replace('{','') + "</td>\
            \n<td><a class='more'> &#9776;</a><div class=extrainfos>"+showbibtex+"</div> <a class='more title'>"+ a['title'].replace('}','').replace('{','') + " </a> <div class=extrainfos><br>"+abstract+"</div></td>\
            \n<td style='display:none;'>"+a['tags']+"</td>\
            \n<td><a class='more journal'>"+ getsource(a) + "</a> <div class=extrainfos>"+sourceinfo+"</div></td>\
            \n<td class=id>"+ a['ID'] + "</td>\
            \n<td class=timestamp>"+ a['timestamp'] + "</td>\
            \n<td class=links>" + filelink + "</td>\
            \n<td class=getlink>"+adslink+"</td>\
            \n<td class=getlink>"+arxivlink+"</td>\
            \n<td class=getbibtexentry>"+adsbibtexlink+"</td>\
            \n<td class=getlink>"+hallink+"</td>\
            \n<td class=getbibtexentry>"+warning+"</td>\
            \n</tr>"
#            \n<!--<tr class=trextrainfos><td colspan=13 class=tdextrainfos>"+extrainfos+"</td></tr>--></tbody>"
        #&#9776;

    #\n<td class=links>"+ paperlinks + "</td>\

    entrytable += "\n</tbody></table></center><br><br><br><br><br><br><br></div>"
    
    return entrytable, nresults
#-----------------------------------------------------------------
def queryobject(args, bib_database):
    result_table = Ned.get_table(args, table='references')
    print('{} references in NED'.format(len(result_table)))
    res = set([x['Refcode'].decode('utf-8') for x in result_table])
    tabres = set([x['bibcode'] for x in bib_database.entries])
    refs = res.intersection(tabres)
    print('{} matches'.format(len(refs)))
    return [x['ID'] for x in bib_database.entries if x['bibcode'] in refs]
#-----------------------------------------------------------------
def getpdf(entry, guess=True):
    print(entry)
    #if guess is '', the default value will be none (i.e., skip)
    if guess is False:
        guess='none'
    else:
        guess = adsquery(entry, get=['pdflink'])
        
    link = adsquery(entry, get=['abstractlink'])
   
    #loop until abort or file found (note: test is always False, but function will just return when successfull)
    test = False
    while not test:
        
        #pdflocation = input(StrAnsicol('PDF (local/remote) location [abort: a; none: n]\n[Default: '+guess+']: ', '?'))
        pdflocation = inputAC(StrAnsicol('PDF (local/remote) location [abort: a; none/keep: n, g: goto webpage]\n[Default: '+guess+']: ', '?'))
        #print(pdflocation)
        #nothing entered, we go ahead with the default (guess) value
        if pdflocation == '':
            pdflocation = guess

        elif pdflocation in ('g','goto'):
            if type(entry) is dict:
                if 'doi' in entry.keys():
                    call([defaultbrowser, 'https://ui.adsabs.harvard.edu/link_gateway/{}/doi:{}'.format(adsquery(entry, get=['bibcode']), entry['doi'])])
                else:
                    call([defaultbrowser, link])
            else:
                call([defaultbrowser, link])
            continue
            
        elif pdflocation in ('a','abort'):
            printc('Aborting...','!!!')
            sys.exit()
        
        elif pdflocation in ('n','none'):
            return 'none'
        
        if 'http' in pdflocation: #remote filename
            printc('Remote file...','!')
            try:
                #replace with ?
                #import urllib.request
                #url = 'https://iopscience.iop.org/article/10.3847/1538-4357/ab16cf/pdf'
                #urllib.request.urlretrieve(url, 'try.pdf')  
                #dl_file = download_file(requests.head(pdflocation, allow_redirects=True).url, cache=False, show_progress=True) #allows redirects from ADS link
                dl_file = download_file(pdflocation, cache=False, show_progress=True)
                return dl_file
            except:
                printc('File could not be downloaded...','!!!')
                continue #failed, ask again for file until success or abort
        else: #local filename
            printc('Local file...','!')
            pdflocation = pdflocation.replace('~', str(Path.home()))
            if not Path(pdflocation).is_file():
                printc('File does not exist...','!!!')
                #continue #failed, ask again for file until success or abort
                call([defaultbrowser, link])
                continue
            return pdflocation
        
#-----------------------------------------------------------------
def isreview(entry):
    #print(entry.keys())
    if 'journal' in entry.keys():
        if ('araa' in entry['journal'].lower()) or ('review' in entry['journal'].lower()):
            return True
    if 'book' in entry['ENTRYTYPE'].lower():
        return True
    if 'booktitle' in entry.keys():
        return True
    if 'abstract' in entry.keys():
        if 'this review' in entry['abstract'].lower():
            return True
    return False
#-----------------------------------------------------------------
def ispreprint(entry, check='preprints'):
    #test true if probably a preprint 
    #check if journal field is arxiv
    if 'journal' in entry.keys():
        if 'arxiv' not in entry['journal'].lower():
            return False #not an arxiv preprint
    #check if book or thesis
    if 'booktitle' in entry.keys() or 'phdthesis' in entry['ID']: 
        return False
    #checking if publication is new or old
    if check=='preprints' and int(entry['year'])<(datetime.datetime.now().year-1):
        return False
    return True
#----------------------------------------------------------------- 
def organizepdfs(bib_database):
    if not os.path.exists(PathBiblio):
        os.makedirs(PathBiblio)
    for i,a in enumerate(bib_database.entries):
        
        filefromfield = parsefilefield(a)
        #move file from file field to expected name from ID
        if filefromfield!='':
            outdir = getlocalpdfname(a)['directory']
            if not os.path.exists(outdir):
                os.makedirs(outdir)
            newfilelocation = getlocalpdfname(a)['filelocation']
            newfieldvalue = getlocalpdfname(a)['fieldvalue']
            if filefromfield == newfilelocation:
                #print('Skipping...')
                continue
            try:
                filefromfield = filefromfield.replace('{','').replace('}','')
                if Path(filefromfield).is_file():
                    shutil.move(filefromfield, newfilelocation)
                    a['file'] = newfieldvalue
                #print(a['ID'] + ': copy successful')
                #print(a['file'])
            except:
                print(StrAnsicol(a['ID'],'@') + StrAnsicol(' : copy failed','!!!'))
                printc(filefromfield,'!!!')
                try:
                    filefromfield = remove_accents(filefromfield)
                    shutil.move(filefromfield, newfilelocation)
                    a['file'] = newfieldvalue
                    #print(a['ID'] + ': copy successful')
                except:
                    print(StrAnsicol(a['ID'],'@') + StrAnsicol(' : copy failed','!!!'))
                    printc(filefromfield,'!!!')
                    printc('Updating field anyway, you should copy manually the file...','!!!')
                    a['file'] = newfieldvalue
    return
#-----------------------------------------------------------------
def updatefromADS(entry, bib_database, check='preprints'):

    orig_entry = deepcopy(entry)
    orig_ispreprint = ispreprint(orig_entry, check=check)
    
    if (check=='all') or (check=='preprints' and orig_ispreprint) or (check=='allpreprints' and orig_ispreprint) or (check!='all' and check!='preprints' and check!='allpreprints'):
    #if orig_ispreprint or 'adsurl' not in entry.keys() or forceADScheck:       
        
        print('\n'+StrAnsicol(entry['ID'],'@') + StrAnsicol(' : checking new information from ADS...','!'))
        #s = getadslink(entry, bibtexentry=True, getcontent=True)['res'] #vian
        s = adsquery(entry, get=['bibtex'])
        if s is None:
            return orig_entry
        #print(s)
        #print(s.count('{'), s.count('}'))
        parser = BibTexParser()
        #parser.customization = convert_to_unicode
        parser.homogenize_fields = False
        s = fixfields(s)
        bib_ads=bibtexparser.loads(s, parser=parser)
        
        try:
            #pp = pprint.PrettyPrinter(indent=4)
            #pp.pprint(bib_ads.entries[0])
            bib_ads.entries[0]
        except:
            print(StrAnsicol(entry['ID'],'@')+StrAnsicol(' : no result with ADS...','!'))
            if 'adsurl' in entry.keys():
                if entry['adsurl'] == 'ignore':
                    print(StrAnsicol(entry['ID'],'@')+StrAnsicol(' : adsurl field is set to ignore, skipping...','!'))
                    return orig_entry
            authorsearch = remove_accents(getfirstauthor(entry)['full']).replace('{','').replace('}','')
            call([defaultbrowser, "http://{}/cgi-bin/nph-abs_connect?db_key=AST&db_key=PRE&qform=AST&arxiv_sel=astro-ph&arxiv_sel=cond-mat&arxiv_sel=cs&arxiv_sel=gr-qc&arxiv_sel=hep-ex&arxiv_sel=hep-lat&arxiv_sel=hep-ph&arxiv_sel=hep-th&arxiv_sel=math&arxiv_sel=math-ph&arxiv_sel=nlin&arxiv_sel=nucl-ex&arxiv_sel=nucl-th&arxiv_sel=physics&arxiv_sel=quant-ph&arxiv_sel=q-bio&sim_query=YES&ned_query=YES&adsobj_query=YES&aut_req=YES&aut_logic=OR&obj_logic=OR&author=%5E{}&object=&start_mon=&start_year={x}&end_mon=&end_year={}&ttl_logic=OR&title=&txt_logic=OR&text=&nr_to_return=200&start_nr=1&jou_pick=ALL&ref_stems=&data_and=ALL&group_and=ALL&start_entry_day=&start_entry_mon=&start_entry_year=&end_entry_day=&end_entry_mon=&end_entry_year=&min_score=&sort=SCORE&data_type=SHORT&aut_syn=YES&ttl_syn=YES&txt_syn=YES&aut_wt=1.0&obj_wt=1.0&ttl_wt=0.3&txt_wt=3.0&aut_wgt=YES&obj_wgt=YES&ttl_wgt=YES&txt_wgt=YES&ttl_sco=YES&txt_sco=YES&version=1".format(adsmirror, authorsearch, str(entry['year']), str(entry['year']))])
            print(str(entry['title']))
            adsbibtex = input(StrAnsicol("Paste url of bibtex entry [ignore='']: ",'?')) #multiline input not possible
            if adsbibtex in ('','ignore'):
                printc('Ignoring for the future: adding adsurl field...','!')
                entry['adsurl'] = 'ignore'
                return entry
            if 'BIBTEX' in adsbibtex and 'BIBTEXPLUS' not in adsbibtex:
                adsbibtex = adsbibtex.replace('BIBTEX','BIBTEXPLUS')
            parser = BibTexParser()
            #parser.customization = convert_to_unicode
            parser.homogenize_fields = False
            bib_ads=bibtexparser.loads(getbibtex(adsbibtex), parser=parser)
            try:
                printc('Record found: ','!')
                pp = pprint.PrettyPrinter(indent=4)
                pp.pprint(bib_ads.entries[0])
            except:
                print(StrAnsicol(entry['ID'],'@')+StrAnsicol(' : no result -again- with ADSL...','!'))
                return orig_entry

#        if bib_ads.entries[0]['title'].lower()!=orig_entry['title'].lower():
        if not StrSimilar(bib_ads.entries[0]['title'].lower(), orig_entry['title'].lower(), threshold=0.75):
            print(StrAnsicol(entry['ID'],'@')+StrAnsicol(' : Title is different...','!'))
            print('Original : ', orig_entry['title'])
            print('New      : ', bib_ads.entries[0]['title'])
            skip = input(StrAnsicol('Skip [Y/n]? ','?'))
            if skip in ('Y', 'y', ''):
                return orig_entry           
            
            
        print(StrAnsicol(entry['ID'],'@')+StrAnsicol(' : updating all fields...','!'))
        
        entry['bibcode'] = bib_ads.entries[0]['ID']
        
        for f in ('ENTRYTYPE', 'author', 'title', 'year', 'doi', 'eid', 'journal', 'keywords', 'month', 'pages', 'volume', \
                  'primaryClass', 'eprint', 'archivePrefix', 'adsurl', 'adsnote', 'series', 'booktitle', 'chapter', \
                  'editor', 'issn', 'school', 'abstract', 'arxivid', 'number', 'eprintclass', 'eprinttype', 'isbn', 'language', \
                  'pmid', 'publisher', 'address', 'institution', 'organization', 'shorttitle', 'type', 'edition', 'key', \
                  'SCHOOL', 'AUTHOR', 'URL', 'YEAR', 'MONTH', 'KEYWORDS', 'TYPE', 'PDF', 'HAL_ID', 'HAL_VERSION'):
            
            if f in bib_ads.entries[0].keys():
                #print(f)
                #print(entry[f])
                #print(bib_ads.entries[0][f])
                entry[f] = bib_ads.entries[0][f]
                if f=='abstract':
                    if bib_ads.entries[0]['abstract'].count('{')!=bib_ads.entries[0]['abstract'].count('}'):
                        print(StrAnsicol(entry['ID'],'@')+StrAnsicol(' : problem with number of opening/closing brackets, removing them...','!!!'))
                        entry['abstract'] = entry['abstract'].replace('}','').replace('{','')
            
            if 'journaltitle' in entry.keys():
                del entry['journaltitle']
            
            if 'keyword' in entry.keys():
                del entry['keyword']
                
        #if not interactive:
        #    if 'journal' in orig_entry.keys():
        #        entry['journal'] = orig_entry['journal'] #so we can come back to it later
        #    print(StrAnsicol(entry['ID'],'@') + StrAnsicol(': update complete','!'))
        #    return entry, True
        
        print(StrAnsicol(entry['ID'],'@')+StrAnsicol(' : checking if new publication data...','!'))
        if orig_entry['year'] > entry['year']: #year is new
            print(StrAnsicol(entry['ID'],'@')+StrAnsicol(' : new publication year','!'))
            print('Was '+ str(orig_entry['year']) + ', now ' + str(entry['year']))
            newid = input(StrAnsicol('Entry will be updated. Update ID as well [Y/n]? ','?'))
            if newid in ('Y', 'y', ''):
                printc('Old ID: ' + entry['ID'],'!')
                entry['ID'] = createcitekey(entry, entries=bib_database.entries, year=True)
                entry['ID'] = createcitekey(entry, entries=bib_database.entries, year=True)
                printc('New ID: ' + entry['ID'],'!')
                printc('Reorganizing PDFs...','!')
                organizepdfs(bib_database)
                #printc('New filename: '+parsefilefield(entry['file']),'!')
        
        if orig_ispreprint and not ispreprint(entry, check=check): #journal was updated
            print(StrAnsicol(entry['ID'],'@')+StrAnsicol(' : new source','!'))
            #call([defaultbrowser, getadslink(entry)['res']])
            #call([defaultbrowser, adsquery(entry, get=['abstractlink'])])
            #pdfresult = getpdf()
            pdfresult = getpdf(entry)
            if pdfresult == 'none':
                printc('Keeping old PDF...','!')
            else:
                filelocation = getlocalpdfname(entry)['filelocation']
                #shutil.copyfile(pdfresult, filelocation)
                #breakpoint()
                shutil.move(pdfresult, filelocation)
                entry['file'] = getlocalpdfname(entry)['fieldvalue']
                printc('Local filename: '+filelocation, '!')
        print(StrAnsicol(entry['ID'],'@')+StrAnsicol(' : update complete','!'))
    return entry
#-----------------------------------------------------------------
def modifyentry(arg, bib_database):
    
    allids = [bib_database.entries[x]['ID'] for x,b in enumerate(bib_database.entries)]
    if arg not in allids:
        printc('ID was not found...','!!!')
        sys.exit()
        
    #get index of arg in allids
    ind = allids.index(arg)
    
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(bib_database.entries[ind])
    
    field = input(StrAnsicol('Field to update [default=file]: ','?'))
    if field == '':
        field = 'file'
        
    if field not in bib_database.entries[ind].keys():
        printc('Field does not exist...','!!!')
        bib_database.entries[ind][field] = ''
        #sys.exit()
    
    if field == 'file':
        #call([defaultbrowser, getadslink(bib_database.entries[ind])['res']])
        #call([defaultbrowser, adsquery(bib_database.entries[ind], get=['abstractlink'])])
        pdfresult = getpdf(bib_database.entries[ind])
        if pdfresult == 'none':
            printc('No PDF...','!')
        else:
            outdir = getlocalpdfname(bib_database.entries[ind])['directory']
            if not os.path.exists(outdir):
                os.makedirs(outdir)
            filelocation = getlocalpdfname(bib_database.entries[ind])['filelocation']
            #shutil.copyfile(pdfresult, filelocation)
            shutil.move(pdfresult, filelocation)
            printc('Local filename: '+filelocation,'!')
            bib_database.entries[ind]['file'] = getlocalpdfname(bib_database.entries[ind])['fieldvalue']
            #bib_ads.entries[0]['url'] = PathBiblio+outname
    elif field == 'ID':
        newvalue = input(StrAnsicol('New ID: ','?'))
        bib_database.entries[ind]['ID'] = newvalue
        #rename PDF if it exists
        filelocation = getlocalpdfname(bib_database.entries[ind])['filelocation']
        if filelocation.strip()!='':
            shutil.move(parsefilefield(bib_database.entries[ind]), filelocation)
            printc('New local filename: ' + filelocation,'')
            bib_database.entries[ind]['file'] = getlocalpdfname(bib_database.entries[ind])['fieldvalue']
    else:
        newvalue = input(StrAnsicol('New value: ','?'))
        bib_database.entries[ind][field] = newvalue
    
    printc('Entry was updated!','!')
#-----------------------------------------------------------------
def viewentry(arg, bib_database):
    allids = [bib_database.entries[x]['ID'] for x,b in enumerate(bib_database.entries)]
    if arg not in allids:
        printc('ID was not found...','!!!')
        sys.exit()
    
    #get index of arg in allids
    ind = allids.index(arg)    
    
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(bib_database.entries[ind])
    
    return
#-----------------------------------------------------------------
def removeentry(args, bib_database):
        
    allids = [bib_database.entries[x]['ID'] for x,b in enumerate(bib_database.entries)]

    for arg in args:
    
        if arg not in allids:
            printc('ID was not found...','!!!')
            sys.exit()
            
        #get index of arg in allids
        ind = allids.index(arg)
        
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(bib_database.entries[ind])
        
        confirm = input(StrAnsicol('Confirm removal [y/N]? ','?'))
        if confirm in ('Y','y'):
            filelocation = getlocalpdfname(bib_database.entries[ind])['filelocation']
            #file = parsefilefield(bib_database.entries[ind])
            printc('Removing PDF: '+filelocation,'!')
            if FileExists(filelocation):
                os.remove(filelocation)
            else:
                printc('File does not exist...', '!!')
            printc('Removing entry: '+bib_database.entries[ind]['ID'],'!')
            del bib_database.entries[ind]
            
    return
#-----------------------------------------------------------------
def arxiv2bibtex(arxivcode, confirm1stauthor=True):
    #headers = {'Authorization': 'Bearer:{}'.format(adstoken)} 
    query = {'id_list': arxivcode}
    encoded_query = urlencode(query, quote_via=quote_plus)
    print("http://export.arxiv.org/api/query?{}".format(encoded_query))
    result = feedparser.parse("http://export.arxiv.org/api/query?{}".format(encoded_query))
    authors = ''
    for i,a in enumerate(result['entries'][0]['authors']):
        if i>0:
            authors += ' and '
        tmp = a['name']
        if '.' in tmp:
            tmp = tmp[tmp.index(".")+1:]
        else:
            tmp = tmp[tmp.index(" ")+1:]
        authors += tmp
    #print(authors)
    #print(result['entries'][0]['title'])
    #print(result['feed']['updated'])    
    #print(result['entries'][0]['summary'])
    firstauthor = result['entries'][0]['authors'][0]['name']
    print(firstauthor)
    if '.' in firstauthor:
        firstauthor = firstauthor[firstauthor.index(".")+1:]
    else:
        firstauthor = firstauthor[firstauthor.index(" ")+1:]
    if confirm1stauthor:
        print('Parsed first author last name (for pdf file name): {}'.format(firstauthor))
        answer = input(StrAnsicol("Change or press enter to continue: ", "?"))
        if answer!="":
            firstauthor = answer
    firstauthor = firstauthor.strip()
    bibtex = '''@unpublished{{{},
firstauthor = {{{}}},
author = {{{}}},
title = {{{}}},
year = {{{}}},
month = {{n/a}},
volume = {{n/a}},
abstract = {{{}}},
eprint = {{arXiv:{}}},
}}'''.format(arxivcode, firstauthor, authors, result['entries'][0]['title'], result['feed']['updated'][0:4], result['entries'][0]['summary'], arxivcode)
    #print(bibtex)
    return bibtex
#-----------------------------------------------------------------
def addentry(args, bib_database, unsaved=False):
    
    for arg in args:

        arg = arg.strip()

        #s = getadslink(arg, bibtexentry=True, getcontent=True) #returns res and pdflink
        s = adsquery(arg, get=['bibtex'])

        #test for arxiv2bibtex
        #s = None
        
        if s is None:
            printc("Could not find ADS entry...",'!')
            
            pubcodetype, pubcode = parsepubcode(arg)
            if pubcodetype=='arxiv':
                s = arxiv2bibtex(arg)
                #print(s)
            else:
                #ask only if this is not a saved entry already
                if not unsaved:
                    answer = input(StrAnsicol("Keep for later? ",'?'))
                    if answer.lower() in ('yes','y'):
                        print('keeping...')
                        with open('bibtexendum_later.txt', 'a') as f:
                            f.write(arg + "\n")
                            print('Entry was saved for later...','!')
                            return 'saved'
                    else:
                        print('Skipped')
                        return 'skipped'
            
        parser = BibTexParser()
        #parser.customization = convert_to_unicode
        parser.homogenize_fields = False
        s = fixfields(s)
        bib_ads=bibtexparser.loads(s, parser=parser)
        try:
            bib_ads.entries[0]['title']
        except:
            printc('Query failed...','!!!')
            sys.exit()

        #e.g., for ASSL books
        if 'author' not in bib_ads.entries[0].keys():
            bib_ads.entries[0]['author'] = bib_ads.entries[0]['editor']

        printc('Title: ' + bib_ads.entries[0]['title'],'!')
        printc('First author: ' + getfirstauthor(bib_ads.entries[0])['full'],'!')
        citekey = createcitekey(bib_ads.entries[0], entries=bib_database.entries, year=True)
        print(StrAnsicol('Computed citekey: ','!') + StrAnsicol(citekey,'@'))

        #modify = input("Modify [y/N]? ")
        #if modify == 'y':
        #    citekey = input("Input desired citekey: ").strip()
        bib_ads.entries[0]['bibcode'] = bib_ads.entries[0]['ID']
        bib_ads.entries[0]['ID'] = citekey

        #input tags
        if defaulttags is not None:
            #bib_ads.entries[0]['tags'] = ''
            tagentry(bib_ads.entries[0])

        #download/copy pdf
        #pdfresult = getpdf(guess=s['pdflink'])
        pdfresult = getpdf(arg)
        print(pdfresult)
        if pdfresult == 'none':
            printc('Adding entry wih no PDF...','!')
        else:
            outdir = getlocalpdfname(bib_ads.entries[0])['directory']
            if not os.path.exists(outdir):
                os.makedirs(outdir)

            filelocation = getlocalpdfname(bib_ads.entries[0])['filelocation']
            #shutil.copyfile(pdfresult, filelocation)
            shutil.move(pdfresult, filelocation)
            printc('Local filename: '+filelocation,'!')
            bib_ads.entries[0]['file'] = getlocalpdfname(bib_ads.entries[0])['fieldvalue']
            #bib_ads.entries[0]['url'] = PathBiblio+outname
        #appena and update
        bib_ads.entries[0]['timestamp'] = '{:%Y-%m-%d}'.format(datetime.datetime.now())
        bib_database.entries.append(bib_ads.entries[0])
        print(StrAnsicol('Entry was added: ','!')+StrAnsicol(citekey,'@'))

        update(bib_database) # update now in case one entry fails later

        if myname.split(',')[0] in bib_ads.entries[0]['author']:
            call([defaultbrowser, 'https://hal.archives-ouvertes.fr/submit/index'])

        if len(args)==1: #for status for f4later
            return 'added'
#----------------------------------------------------------------- 
def duplicates(bib_database):
    
    allids = [bib_database.entries[x]['ID'] for x,b in enumerate(bib_database.entries)]
    res = GetDuplicates(allids, getIndices=True)
    if len(res)>0:
        printc('Duplicate IDs were found, please fix problem first...','!!!')
        print(res)
        sys.exit()
    
    allentries = [bib_database.entries[x]['firstauthor']+':'+bib_database.entries[x]['title'] for x,b in enumerate(bib_database.entries)]
    res = GetDuplicates(allentries, getIndices=True)
    if len(res)>0:
        printc('Duplicate entries were found...','!')
        #get index of arg in allids
        for r in res:
            printc('\n'+r,'!!!')
            for i in res[r]:
                printc(bib_database.entries[i]['ID'],'@')
        return True
    
    return False
#----------------------------------------------------------------- 
def checkincomplete(bib_database):
    some = False
    for i,a in enumerate(bib_database.entries):
        if 'doi' not in a.keys() and 'eprint' not in a.keys() and 'bibcode' not in a.keys():
            print(StrAnsicol(a['ID'],'@')+StrAnsicol(' : entry is incomplete','!!!'))    
            some = True
        if 'bibcode' not in a.keys():
            print(StrAnsicol(a['ID'],'@')+StrAnsicol(' : bibcode is missing','!!!'))    
    if some:
        printc('Consider editing manually the .bib file','!')
    return
#-----------------------------------------------------------------
def makeorgfile(filename, bib_database, args=None, where=None, iwhere = False):
    firsttable, nresults = makeorgtable(bib_database.entries, itemgetter('timestamp', 'author', 'year'), reverse=True, name='timestamp', where=where, iwhere = iwhere)

    with open(filename+'.org', 'w') as f:
        f.write(firsttable)
        
#-----------------------------------------------------------------
def makepage(filename, bib_database, args=None, where=None, iwhere = False):
    
    firsttable, nresults = maketable(bib_database.entries, itemgetter('timestamp', 'author', 'year'), reverse=True, name='timestamp', where=where, iwhere = iwhere)

    if args is not None:
        firsttable = '<b>Query:</b> {}<br><br>'.format(''.join(args)) + firsttable

    firsttable = '<br><b>Number of entries:</b> {}<br>'.format(nresults) + firsttable
        
    with open('bibtexendum_template.html', 'r') as myfile:
        template = myfile.readlines()
    template = "".join(template)

    text1 = 'TAGS: <small>Review --- '
    for d in defaulttags:
        text1 += "<font color=yellow>{}</font>: <i>".format(d)
        for dd in defaulttags[d]:
            text1 += dd + " "
        text1 += "</i> --- "
    text1 += "</small>"

    with open(filename+'.html', 'w') as f:
        f.write(template.replace('{fill_here1}', text1).replace('{fill_here2}', firsttable))
#-----------------------------------------------------------------
def checkown(bib_database, name=myname):
    papers = ADS_getmypapers(name=name, update=False, refereed=True)
    if papers is None:
        print('No results...')
        return
    bibcodes = [a['bibcode'] for a in bib_database.entries]
    for p in papers:
        if p.bibcode not in bibcodes:
            print('\n{}+ ({}) [{}]: {}'.format(p.first_author, p.year, p.bibcode, p.title))
            answer = input(StrAnsicol("Add entry? ",'?'))
            if answer.lower() in ('yes','y'):
                addentry([p.bibcode], bib_database)
                update(bib_database)
            
#-----------------------------------------------------------------
#-----------------------------------------------------------------
#-----------------------------------------------------------------
#-----------------------------------------------------------------
#-----------------------------------------------------------------
#-----------------------------------------------------------------


def update(bib_database, resetcitekeys=False, check=None):
        
    #print(bib_database.entries[1].keys())
    #bib_database.entries[0].items()
    #keylist=()

    allids = [bib_database.entries[x]['ID'] for x,b in enumerate(bib_database.entries)]

    if check=='all':
        if FileExists('checkall_list.txt'):
            with open ('checkall_list.txt', 'r') as f:
                checked = f.readlines()
        else:
            checked = []            
    
    for i,a in enumerate(bib_database.entries):
        #if 'mingyue' not in a['firstauthor'].lower():
        #    continue

        if check=='all':
            if a['ID'] in checked:
                continue
        
        #print(a)
        if a['ENTRYTYPE']=='unpublished':
            print('Updating unpublished {} from ADS'.format(a['ID']))
            updatefromADS(a, bib_database, check='all')
            print(a)
        
        #print(a['ID'])
        
        if check is not None:
            if 'preprints' not in check and 'allpreprints' not in check and 'all' not in check and 'HAL' not in check: #must be for specific ID
                if a['ID'] not in check:
                    continue
                else:
                    print(StrAnsicol('Updating specific entry: ','!')+StrAnsicol(a['ID'],'@'))
        
        #cleaning, update from ADS entry
        if check is not None:
            if check!='HAL':
                bib_database.entries[i] = updatefromADS(a, bib_database, check=check)
                #if not status:
                #    cleanup(bib_database=bib_database)
            
        bib_database.entries[i]['firstauthor'] = getfirstauthor(a)['full']
        
        #create HAL identifier field is not existing
        if 'hal_id' not in a.keys():
            bib_database.entries[i]['hal_id'] = ''
            bib_database.entries[i]['hal_version'] = ''
            
        #create bibcode field is not existing
        if 'bibcode' not in a.keys():
            bib_database.entries[i]['bibcode'] = ''
        
        #create file field is not existing
        if 'file' not in a.keys():
            bib_database.entries[i]['file'] = ''
        else:
            bib_database.entries[i]['file'] = bib_database.entries[i]['file'].replace('//','/')

        #create tags field automatically #is not existing
        #if ('tags' not in a.keys()) and (defaulttags is not None):
        #if defaulttags is not None:
        #    tagentry(bib_database.entries[i])

        #    bib_database.entries[i]['tags'] = ':'.join(sorted(bib_database.entries[i]['tags'].split(':')))
            
        #check if file in bibtex file field is as expected from the ID
        if bib_database.entries[i]['file'].strip()!='':
            filefromfield = parsefilefield(bib_database.entries[i])
            filelocation = getlocalpdfname(bib_database.entries[i])['filelocation']
            if not FileExists(filefromfield) or not FileExists(filelocation):
                print(StrAnsicol(a['ID'],'@') + StrAnsicol(' : missing file? (can happen if updating from ADS and ADS author format is different)', '!!!'))
                print(filefromfield)
                print(filelocation)
                #exit()
                #print(getfirstauthor(bib_database.entries[i]))
                #print(createcitekey(bib_database.entries[i]))
                #print(createcitekey(bib_database.entries[i], entries=bib_database.entries, year=True))
            elif not cmp(filefromfield, filelocation):
                print(StrAnsicol(a['ID'],'@') + StrAnsicol(' : file location indicated in bibtex entry seems to differ...', '!!!'))
                printc('Bibtex file field: '+filefromfield, '!!!')
                printc('Expected location: '+filelocation, '!!!')

        #check if file actually exists, stop it False
        if bib_database.entries[i]['file'].strip()!='':
            if not Path(filefromfield).is_file():
                print(StrAnsicol(a['ID'],'@') + StrAnsicol(' : file could not be found: '+filefromfield,'!!!'))
                printc('Make sure the LibraryName variable contains the right path, and use -o option if necessary...', '!!!')
            
        #try to fill the date
        if 'year' not in a.keys():
            printc('No year!' + bib_database.entries[i]['ID'],'!!!')
            if 'date' in a.keys():
                bib_database.entries[i]['year'] = a['date'].split('-')[0]
            else:
                printc('Not even a date?!','!!!')
        
        #if 'journal' in a.keys():
        #    bib_database.entries[i]['journal'].replace('&','\&')
        
        if 'abstract' in a.keys():
            if bib_database.entries[i]['abstract'].count('{')!=bib_database.entries[i]['abstract'].count('}'):
                print(StrAnsicol(bib_database.entries[i]['ID'],'@')+StrAnsicol(' : problem with number of opening/closing brackets, removing them...','!!!'))
                bib_database.entries[i]['abstract'] = bib_database.entries[i]['abstract'].replace('}','').replace('{','')
            #remove \n from abstracts
            bib_database.entries[i]['abstract'] = bib_database.entries[i]['abstract'].replace('\n',' ')

                
        #update ID
        if resetcitekeys:
            if createcitekey(a) in bib_database.entries[i]['ID']: #to avoid mixing abcd... when resetting
                printc('ID seems to exist already with the right format, skipping...','!')
            else:
                bib_database.entries[i]['ID'] = createcitekey(a, entries=bib_database.entries, year=True)
        
        #get a timestamp
        if 'timestamp' not in a.keys():
            bib_database.entries[i]['timestamp'] = ''
        if a['timestamp'] == '':
            if 'date' in a.keys():
                bib_database.entries[i]['timestamp'] = a['date']
                del bib_database.entries[i]['date']
            else:
                bib_database.entries[i]['timestamp'] = a['year']
        if a['timestamp'] == '2017':
                bib_database.entries[i]['timestamp'] = '2017-01-01'
                
        #if a['ID']=='Magdis:2013uk':
        #    print(bib_database.entries[i]['link'])
        
        if 'link' not in a.keys():
            bib_database.entries[i]['link'] = ''
        bib_database.entries[i]['link'] = a['link'].replace('{%}5Cn', ' ').replace('{%}0A', ' ')
        
        #remove papers2&3 links
        links = ''
        for il,l in enumerate(bib_database.entries[i]['link'].split(' ')):
            if 'papers2' not in l and 'papers3' not in l:
                if il==0:
                    links += str(l)
                else:
                    links += ' ' + str(l)
        bib_database.entries[i]['link'] = links

        if 'tags' not in a.keys() and defaulttags is not None:
            #bib_ads.entries[0]['tags'] = ''
            tagentry(bib_database.entries[i])
        
        #if bib_database.entries[i]['ID']!='a' and bib_database.entries[i]['ID'][:-1]+'a' not in allids:
        #    answer = input(StrAnsicol("The (a) citekey is missing, reorganize?  ",'?')) 
    
        if check=='HAL':
            #check if in HAL only for my papers
            if myname.split(',')[0] in bib_database.entries[i]['author'].lower():
                #print('Checking HAL entry for {}'.format(a['ID']))
                if bib_database.entries[i]['hal_id']=='':
                    doi = bib_database.entries[i]['doi'] if 'doi' in bib_database.entries[i].keys() else None                    
                    bib_database.entries[i]['hal_id'], bib_database.entries[i]['hal_version'] = HAL_find(doi=doi, title=bib_database.entries[i]['title'], verbose=True)
                
        if check is not None:
            with DelayedKeyboardInterrupt():
                # stuff here will not be interrupted by SIGINT
                with open(LibraryName, 'w') as bibtex_file:
                    bibtexparser.dump(bib_database, bibtex_file)
                    
        with open ('checkall_list.txt', 'w+') as f:
            f.write(a['ID'])

    #allids = [bib_database.entries[x]['ID'] for x,b in enumerate(bib_database.entries)]
        
    #print(keylist)

    #check here if there is xxxxb and no xxxxa and etc...
    #ids = [b['ID'] for b in bib_database.entries]

    #save changes
    timestamp = '{:%Y%m%d_%H%M%S}'.format(datetime.datetime.now())
    printc('\nBacking up old library file...','!')
    backupdir = 'Backup/'
    if not os.path.exists(backupdir):
        os.makedirs(backupdir)
    oldfile = backupdir+LibraryName+'_'+timestamp+'.bib'
    shutil.copyfile(LibraryName, oldfile)
    os.system('bzip2 -zf '+oldfile)
    
    #bib_database.entries = tmp
    printc('Writing new library file...','!')
    with open(LibraryName, 'w') as bibtex_file:
        bibtexparser.dump(bib_database, bibtex_file)
    
    #-----------------------------------------------------------------
    #-----------------------------------------------------------------

    makepage('bibtexendum', bib_database)
    
    
    makeorgfile('bibtexendum', bib_database)


#-----------------------------------------------------------------
#-----------------------------------------------------------------
#-----------------------------------------------------------------
#-----------------------------------------------------------------
#-----------------------------------------------------------------
#-----------------------------------------------------------------

def main():
    
    helpmessage = 'Check README.org'
    #   * Extract keywords from abstracts (e.g., for use in arXiv filters):
    #        python bibtexendum.py -extract
    
    parser = argparse.ArgumentParser(description=helpmessage, formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('--update', '-u', action='store_true', help='Update webpage and clean some empty fields')
    parser.add_argument('--organize', '-o', action='store_true', help='Organize PDF filenames according to citekey/ID for all entries')
    parser.add_argument('--resetcitekeys', '-r', action='store_true', help='make new citekeys based on first author and year (& keep ones that have the right format)')
    parser.add_argument('--incomplete', '-i', action='store_true', help='Check for incomplete entries (no doi, eprint, or bibcode)')
    
    parser.add_argument('--check', '-c', nargs='*', help='Check for journal updates [allpreprints, preprints, all, or specific IDs]', type=str)
    
    parser.add_argument('--extract', '-e', nargs='*', help='Extract keywords from abstracts', type=str)
        
    parser.add_argument('--tags', '-t', action='store_true', help='Force tag updates')
    
    parser.add_argument('--add', '-a', nargs='*', help='Add new entries [doi, arxiv, or bibcode]', type=str)
    #parser.add_argument('--keep', '-k', nargs='*', help='Keep entries for later [doi, arxiv, or bibcode]', type=str)
    parser.add_argument('--delete', '-d', nargs='*', help='Delete entries [IDs]', type=str)
    parser.add_argument('--modify', '-m', nargs=1, help='Modify an existing entry [ID]', type=str)
    parser.add_argument('--view', '-v', nargs=1, help='View an existing entry [ID]', type=str)
    
    parser.add_argument('--query', '-q', nargs=1, help='Query an object name with NED and return matching entries', type=str)
    
    parser.add_argument('--perso', '-p', action='store_true', help='Check if own papers in library')

    parser.add_argument('--search', '-s', nargs='*', help='Search text within PDF library', type=str)
    
    parser.add_argument('args', nargs=argparse.REMAINDER)
    
    args = parser.parse_args()
    
    if args.args:
        print("Too many arguments, check help -h...")
        sys.exit()
    
    #-----------------------------------------------------------------
    #-----------------------------------------------------------------
         
    if args.search is not None:
        print(args.search)
        search()
        exit()
        
    #-----------------------------------------------------------------
    #-----------------------------------------------------------------
    
    printc('\nParsing library file... ({})'.format(LibraryName),'!')
    with open(LibraryName) as bibtex_file:
    #with open('jab_utf8.bib') as bibtex_file:
        parser = BibTexParser()
        #parser.customization = convert_to_unicode
        parser.homogenize_fields = False
        bib_database = bibtexparser.load(bibtex_file, parser=parser)
        #print('Number of entries: {}'.format(len(bib_database.entries)))
       
    global PathBiblio, TildePathBiblio
    tmp = [s for s in bib_database.comments if 'bibtexendum.pdflibrary' in s][0]
    TildePathBiblio = StrBetween(tmp, ('{','}'))
    PathBiblio = os.path.expanduser(TildePathBiblio) #takes care of ~ if present
    #print(PathBiblio)
    
    #C-x = i emacs to check position
    #use kwrite save as with encoding UTD

    update(bib_database) #need to update first in case some entries were added manually
    if duplicates(bib_database):
        printc('Duplicates exist, please fix problem...','!!!')
    else:
        printc('No duplicates found...','!')

    #check if unsaved entries need to be processed
    f4later = 'bibtexendum_later.txt'
    keep = []
    if FileExists(f4later):
        size = os.stat(f4later).st_size
        if size>0:
            printc('Unsaved entries exist...', '!')
            with open(f4later, 'r') as f:
                for line in f: 
                    answer = input('Add {}? '.format(line))
                    if answer.lower() in ('y', 'yes'):
                        status = addentry([line], bib_database, unsaved=True)
                        if status=='saved':
                            keep.append(line)
                        update(bib_database)
                    else:
                        keep.append(line)    
            if keep!=[]:
                with open(f4later, 'w') as f:
                    for k in keep:
                        f.write(k)
            else:
                os.remove(f4later)       
        
    
    #-----------------------------------------------------------------
    #-----------------------------------------------------------------
    
    
    #options with no arguments
    if args.update:
        print("\nUpdating...")
        update(bib_database)
        
    if args.perso:
        print("\nChecking if own papers in library...")
        checkown(bib_database)
        
    if args.organize:
        print("\nOrganizing PDFs...")
        organizepdfs(bib_database)   
        update(bib_database)  
        
    if args.resetcitekeys:
        print("\nResetting citekeys...")
        update(bib_database, resetcitekeys=True)
        
    if args.incomplete:
        print("\nChecking for incomplete entries...")
        checkincomplete(bib_database)
        
    if args.extract:
        print("\nExtract keywords from abstracts (beta)...")
        print("\n (takes a while... few minutes per 1000 entries)")
        from RAKE import rake
        rake_object = rake.Rake("~/WORK/Science/Python/Library/RAKE/SmartStoplist.txt", 3, 3, 5)
        #Each word has at least 3 characters
        #Each phrase has at most 3 words
        #Each keyword appears in the text at least 5 times
        text = ".".join([bib_database.entries[x]['abstract'] for x,b in enumerate(bib_database.entries) if 'abstract' in bib_database.entries[x].keys()])
        text = text.replace("$", "").replace("\\", "").replace("<br />", "").replace("{", "").replace("}", "")
        keywords = rake_object.run(text)
        minscore = 5.
        final = [i for i,x in enumerate(keywords) if x[1]>minscore]
        for i in final:
            print("{}: {}".format(keywords[final[i]][0], keywords[final[i]][1]))
        
    if args.tags:
        print("\nTag entries with no tags...")
        tagentries(bib_database)
        update(bib_database)  
        
    #options with arguments
    if args.add:
        print("\nAdding new entries: ")
        print(args.add)
        addentry(args.add, bib_database)   # update in addentry in case one entry fails
        
    #if args.keep:
    #    print("\nKeeping some entries: ")
    #    print(args.keep)
    #    keepentry(args.keep, bib_database)   
    #    update(bib_database)  
        
    if args.delete:
        print("\nDeleting new entries: ")
        print(args.delete)
        removeentry(args.delete, bib_database)
        update(bib_database)  
        
    if args.modify:
        print("\nModifying an existing entry: ")
        print(args.modify)  
        modifyentry(args.modify[0], bib_database)   
        update(bib_database) 
        
    if args.view:
        print("\nViewing an existing entry: ")
        print(args.view)  
        viewentry(args.view[0], bib_database)   
        #update(bib_database) 
        
    if args.query:
        print("\nQuerying object: ")
        print(args.query)  
        idlist = queryobject(args.query[0], bib_database) 
        makepage('bibtexendum_filter', bib_database, args=args.query, where=('ID', idlist), iwhere=True)  
        #update(bib_database)
        printc('New page created: bibtexendum_filter.html', '!')
        call([defaultbrowser, 'bibtexendum_filter.html'])
        
    if args.check:
        print("\nChecking for updates: ")
        print(args.check)  
        if 'allpreprints' in args.check:
            print('Checking all preprints...')
        elif 'preprints' in args.check:
            print('Checking recent preprints...')
        elif 'all' in args.check:
            print('Checking all entries...')
        elif 'HAL' in args.check:
            print('Checking HAL entries...')
        else:
            print('Checking specific entries...')
        update(bib_database, check=args.check[0])
         
    #-----------------------------------------------------------------
    #-----------------------------------------------------------------
    #-----------------------------------------------------------------
    #-----------------------------------------------------------------
    #-----------------------------------------------------------------
    #-----------------------------------------------------------------
    
    
        

#cp jab_utf8.bib bibtexendum.bib 
#python bibtexendum.py -r
#python bibtexendum.py -f


#CLEANING OLD LIBRARY
#-r: first to get different ids (optional)
#-c all: update all fields from ADS but don't propose to download new PDF
#-r: again to get right citekeys from ADS author field
#-o: organize pdfs according to IDs
            
#MAINTENANCE
#-u, m, a, c etc...




#-----------------------------------------------------------------
#-----------------------------------------------------------------
#-----------------------------------------------------------------
#-----------------------------------------------------------------
#-----------------------------------------------------------------
#-----------------------------------------------------------------

def cleanup(bib_database=None):
    printc('Ending process','!!!')
    if bib_database is not None:
        update(bib_database)
    else:
        answer = input(StrAnsicol("Update database ",'?'))
        if answer.lower() in ('yes','y'):
            update(bib_database)  
    sys.exit(1)
    
#-----------------------------------------------------------------
#-----------------------------------------------------------------
#-----------------------------------------------------------------
#-----------------------------------------------------------------
#-----------------------------------------------------------------
#-----------------------------------------------------------------
    
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupt!')
        #cleanup()
        

